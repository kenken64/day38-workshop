console.log("store server");
const express = require('express'),
       cors = require('cors'),
       bodyParser = require('body-parser'),
       mqtt = require('mqtt');


port = process.env.PORT || 3000;
const app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

const mqttClient  = mqtt.connect({
  host: 'tailor.cloudmqtt.com',
  port: 14487,
  username: 'nptwzvxf',
  password: 'FRQrleLJ1Jia'
});

mqttClient.on('connect', function () {
    mqttClient.subscribe('store/order', function (err) {
    if (!err) {
      app.get("/api/buy", function (req, res) {
        console.log("buy a dog");
        let p  = {
          productName: "dog", 
          quantity: 1
        }
        mqttClient.publish('store/order', JSON.stringify(p))
        res.status(200).json(p);
      });
    }
  })
})

app.listen(port, ()=>{
	console.log(`Listening on port => ${port}`);
});
