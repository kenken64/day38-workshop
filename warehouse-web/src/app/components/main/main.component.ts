import { 
  Component, 
  OnInit ,
  OnDestroy,
  AfterViewInit} from '@angular/core';
import { WarehouseService } from '../../Warehouse.service';
import { Event } from '../../event';
import { fromEvent, Subscription } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {
  mainSubscription: Subscription;
  ioConnection: any;
  quantity: string = '10';

  constructor(private wrSvc: WarehouseService) { 
    this.initIoConnection();
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
  
  }

  ngOnDestroy() {
    this.mainSubscription.unsubscribe();
  }

  private initIoConnection(): void {
    this.wrSvc.initSocket();

    this.ioConnection = this.wrSvc.onMessage()
      .subscribe((message: any) => {
        console.log("???? " + message);
        this.quantity = message;
      });

    this.wrSvc.onEvent(Event.CONNECT) 
      .subscribe(() => {
        console.log('connected');
      });
      
    this.wrSvc.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected');
      });
  }

}
