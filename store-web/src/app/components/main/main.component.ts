import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../store.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  message : string;

  constructor(private storeSvc : StoreService) { }

  ngOnInit() {
  }

  buy(){
    console.log("Buy!");
    this.storeSvc.buy().then((result)=>{
      console.log(result);
    })
  }

}
